class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.date :due_date
      t.string :client_name
      t.string :birth_location
      t.string :birth_address
      t.string :care_provider
      t.string :client_phone
      t.string :client_address
      t.string :client_email
      t.string :partner_name
      t.string :partner_phone
      t.integer :baby_number
      t.string :shot_list
      t.string :no_shot_list
      t.string :birth_history
      t.string :birth_attendees
      t.date :birth_date
      t.boolean :baby_born
      t.boolean :miscarriage

      t.timestamps
    end
  end
end
